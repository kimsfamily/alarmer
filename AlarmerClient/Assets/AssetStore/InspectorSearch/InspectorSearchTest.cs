﻿using UnityEngine;
using System.Collections;



public class InspectorSearchTest : MonoBehaviour
{
#pragma warning disable 0414
//#pragma warning disable 0109
    public Light muzzleFlashLight;
    public Vector3 distanceToCursor = Vector3.one * 99999;
    public Vector3 distanceToCursor2;
    public int TargetPlayerId = -1;
    public Transform FlagPlaceHolder;
    private const int minVel = (100 / 4);
    public Transform rainfall;
    public Transform cam;
    public bool brake;
    //private float groundedTime;
    //private bool movingBack;
    //private bool movingBack2;
    internal Transform leftWhell = null;
    internal Transform rightWhell = null;
    internal Transform upLeftWhell = null;
    internal Transform upRightWhell = null;

    private float tireRot = 0f;
    private float tireRot2 = 0f;

    internal bool grounded2 = true;
    public float Friq = 0.06f;
    private float rotVel = 0f;
    private AudioSource idleAudio = null;
    private AudioSource stopAudio = null;
    private AudioSource motorAudio = null;
    private AudioSource windAudio = null;
    private AudioSource backTimeAudio = null;
    private AudioSource nitroAudio = null;
    internal Vector3 oldpos = Vector3.zero;
    public float totalMeters;
    public float checkPointMeters;
    internal bool up = false;
    internal bool down = false;
    internal bool left = false;
    internal bool right = false;

    public Transform[] camPoses;
    //public new Camera camera; 
    public Camera Camera;
    public Transform model;
    public bool ghost;
    private Vector3 deltaPos = Vector3.zero;
    private Vector3 ghostVel = Vector3.zero;
    public ParticleSystem[] emiters;
    //internal new Rigidbody rigidbody = null;
    internal Rigidbody Rigidbody = null;
    internal new Transform transform = null;
    public Transform flashLight;
    private float hitForce = 0f;
    public int randomHash;
    public Collider carBox;
//#pragma warning restore 0109
#pragma warning restore 0414
}
