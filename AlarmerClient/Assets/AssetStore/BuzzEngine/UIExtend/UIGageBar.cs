﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using System;

namespace BuzzEngine.UI
{
    public class UIGageBar : MonoBehaviour
    {
        public enum TimerType { Increase, Decrease}
        public enum BlinkDrawType { Color, Sprite, }

        public Image Frame;
        public Image Bar;
        public Text Label;
        
        public string Text { get { return Label.text; } set { Label.text = value; } }
        [HideInInspector]
        public TimerType timerType = TimerType.Decrease;
        [HideInInspector]
        public BlinkDrawType blinkDrawType = BlinkDrawType.Color;
        [HideInInspector]
        public List<Sprite> BarSprites = new List<Sprite>();
        [HideInInspector]
        public Color BlinkColor = Color.white;
        [HideInInspector]
        public float FullTime = 60;
        [HideInInspector]
        public bool IsRun = false;
        [HideInInspector]
        public float BlinkRatio = 1;
        [HideInInspector]
        public float BlinkTimeGap = 1;
        [HideInInspector]
        float _blinkTimer = 0f;
        int _blinkId = 0;
        [HideInInspector]
        public string TextFormat = "Input Text {0:0}";

        public Action StopCallback = null;
        [HideInInspector]
        public bool IsAutoText = false;
        [HideInInspector]
        public bool UseBlink = false;

        Color _barColor = Color.white;

        void Awake()
        {
            _barColor = Bar.color;
        }
        /// <summary>
        /// fillAmount of bar, 0 ~ 1
        /// </summary>
        public float FillValue
        {
            get
            {
                return Bar.fillAmount;
            }
            set
            {
                Bar.fillAmount = value;
            }
        }
        /// <summary>
        /// real value, min ~ max
        /// </summary>
        public float Value
        {
            get { return FillValue; }
            set
            {
                FillValue = value;
                if (IsAutoText && Label != null && Label.gameObject.activeSelf)
                {
                    try {
                        Label.text = string.Format(TextFormat, ValueToTime());
                    }catch(FormatException e)
                    {
                        Label.text = "textFormat code error : " + TextFormat;
                        Debug.LogError(e.ToString());
                    }
                }
            }
        }
        
        // Update is called once per frame
        void Update()
        {
            if (IsRun == false) return;

            switch (timerType)
            {
                case TimerType.Increase:
                    Increase();
                    break;
                default:
                    Decrease();
                    break;
            }
        }
        void Increase()
        {
            if (Value < 1f)
            {
                Value += Time.deltaTime / FullTime;

                // blinkRatio 수치 이하 부터 작동 한다.
                if (UseBlink && Value >= BlinkRatio) Blink();
            }
            else
            {
                Value = 1f;
            }
        }

        void Decrease()
        {
            if( Value > 0f)
            {
                Value -= Time.deltaTime / FullTime;

                // blinkRatio 수치 이하 부터 작동 한다.
                if (UseBlink && Value <= BlinkRatio) Blink();
            }
            else
            {
                Value = 0f;
            }
            
        }
        
        void Blink()
        {
            _blinkTimer -= Time.deltaTime;

            if (_blinkTimer <= 0)
            {
                if (blinkDrawType == BlinkDrawType.Color)
                {
                    Bar.color = Bar.color == BlinkColor ? _barColor : BlinkColor;
                }
                else
                {
                    // 깜빡이 시스템을 사용 할지 스프라이트의 존재를 통해 확인한다.
                    if (BarSprites == null || BarSprites.Count == 0) return;

                    Bar.sprite = BarSprites[_blinkId];
                    _blinkId = _blinkId + 1 >= BarSprites.Count ? 0 : _blinkId + 1;
                }
                
                if (BlinkRatio <= 0 || BlinkRatio >= 1)
                    _blinkTimer = BlinkTimeGap;
                else
                    _blinkTimer = BlinkTimeGap * (Value * 0.5f);
            }
        }
        /// <summary>
        /// fillValue zero
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return Value <= 0 ? true : false;
            }
        }
        /// <summary>
        /// fillValue > 1
        /// </summary>
        public bool IsFull
        {
            get
            {
                return Value >= 1 ? true : false;
            }
        }

        public float ValueToTime()
        {
            return FullTime * FillValue;
        }
        public void StartTimer(float _fullTime, Action callback, bool active = true)
        {
            gameObject.SetActive(active);

            IsRun = true;
            FullTime = _fullTime;

            Value = timerType == TimerType.Increase ? 0 : 1f;

            StopCallback = callback;            
        }

        public void PauseTimer()
        {
            IsRun = false;
        }
        public virtual void StopTimer(bool active = true)
        {
            if (IsRun == false) return;

            IsRun = false;
            if (StopCallback != null) StopCallback();

            if (blinkDrawType == BlinkDrawType.Color)
            {
                Bar.color = _barColor;
            }
            else
            {
                // 깜빡이 시스템을 사용 할지 스프라이트의 존재를 통해 확인한다.
                if (BarSprites != null && BarSprites.Count > 0)
                {
                    Bar.sprite = BarSprites[0];
                }

                
            }

#if UNITY_EDITOR
            Debug.Log(gameObject.name + " Timer Stop");
#endif

            gameObject.SetActive(active);

        }

        public void Visable(bool val)
        {
            Bar.enabled = val;
            Frame.enabled = val;
            Label.enabled = val;
        }

        public void Refresh()
        {
            Value = Value;
            Bar.SetAllDirty();
        }
    }
}