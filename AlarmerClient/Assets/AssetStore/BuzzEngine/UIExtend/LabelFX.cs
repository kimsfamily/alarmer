﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BuzzEngine.UIExtend
{
    public class LabelFX : FSM.FSMMono<LabelFX>
    {
        public enum eFXType
        {
            Scroll,Flicker, Typing
        }
        
        public eFXType StartState;
        public bool Flag { get; set; }
        public float Time = 0.1f;
        public float Interval = 0.05f;
        public float LoopingTerm = 1f;
        public bool AutoStart = false;
        public Text Label;
        public string Text { get; set; }
        public bool IsLoop = false;

        IEnumerator _currentCoroutine;

        void Awake()
        {
            if(AutoStart) Play(StartState, Label.text, IsLoop);    
        }

        public Coroutine Play(eFXType type, string text, bool isLoop = false)
        {
            if(_currentCoroutine != null) StopCoroutine(_currentCoroutine);
            
            Flag = true;
            Text = text;
            IsLoop = isLoop;
            ChangeState(this, type);

            return StartCoroutine(_currentCoroutine = ((LabelFXState)State).Play(text));
        }

        public Coroutine Stop(string text, float endDelay = 0f)
        {
            if (_currentCoroutine != null) StopCoroutine(_currentCoroutine);

            return StartCoroutine(iStop(text, endDelay));
        }
        IEnumerator iStop(string text, float endDelay = 0f)
        {
            yield return StartCoroutine(((LabelFXState)State).Stop(text, endDelay));

            Flag = false;
        }

        public void StopQuick(string text)
        {
            if (_currentCoroutine != null) StopCoroutine(_currentCoroutine);
            Label.text = text;
            Flag = false;
        }
    }
    public abstract class LabelFXState : FSM.FSMMonoState<LabelFX>
    {
        public abstract IEnumerator Play(string text);
        public abstract IEnumerator Stop(string text, float endDelay = 0f);
    }

    public class Scroll : LabelFXState
    {
        public override IEnumerator Play(string text)
        {
            yield return null;
        }

        public override IEnumerator Stop(string text, float endDelay = 0f)
        {
            yield return new WaitForSeconds(endDelay);
        }
    }
    public class Flicker : LabelFXState
    {
        public override IEnumerator Play(string text)
        {
            yield return null;
        }

        public override IEnumerator Stop(string text, float endDelay = 0f)
        {
            yield return new WaitForSeconds(endDelay);
        }
    }
    public class Typing : LabelFXState
    {
        public override IEnumerator Play(string text)
        {
            Mono.Label.text = "";

            var len = text.Length;

            while (Mono.Flag)
            {
                for (var i = 0; i < len && Mono.Flag; i++)
                {
                    Mono.Label.text = text.Substring(0, i + 1);

                    yield return new WaitForSeconds(Mono.Interval);
                }

                yield return new WaitForSeconds(Mono.LoopingTerm);

                Mono.Flag = Mono.IsLoop;
            }
        }

        public override IEnumerator Stop(string text,float endDelay = 0f)
        {
            var start = Mono.Label.text.Length;
            var len = text.Length;

            for (var i = start; i < len; i++)
            {
                Mono.Label.text = text.Substring(0, i + 1);

                yield return new WaitForSeconds(Mono.Interval);
            }
            yield return new WaitForSeconds(endDelay);
        }
    }
}