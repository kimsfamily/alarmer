﻿using BuzzEngine.Editor;
using BuzzEngine.UI;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UI;

namespace BuzzEngine.UIExtend.Editor
{
    [CustomEditor(typeof(UIGageBar))]
    public class UIGageBarEditor : BzEditor
    {
        public UIGageBar gage = null;
        Canvas canvas = null;

        //SerializedProperty fullTime;

        protected override void OnEnable()
        {
            base.OnEnable();
            gage = (UIGageBar)target;
            showDefaultVariablies = true;
            FindCanvas();

            //fullTime = serializedObject.FindProperty("fullTime");

        }
        void FindCanvas()
        {
            canvas = gage.gameObject.GetComponent<Canvas>();
            Transform target = gage.transform;

            while (canvas == null)
            {
                // 검사 후 캔버스가 없다면 하나 만들어 준다.   
                if ( target.parent == null)
                {
                    Canvas[] canvasList = GameObject.FindObjectsOfType<Canvas>();

                    canvas = gage.gameObject.AddComponent<Canvas>();
                    CanvasScaler scaler = gage.gameObject.AddComponent<CanvasScaler>();

                    foreach (Canvas c in canvasList)
                    {
                        canvas.renderMode = c.renderMode;
                        canvas.worldCamera = c.worldCamera;
                        canvas.planeDistance = c.planeDistance;
                        canvas.sortingLayerID = c.sortingLayerID;
                        canvas.sortingOrder = c.sortingOrder;

                        CanvasScaler cs = c.GetComponent<CanvasScaler>();
                        if(cs != null)
                        {
                            scaler.uiScaleMode = cs.uiScaleMode;
                            scaler.referenceResolution = cs.referenceResolution;
                            scaler.screenMatchMode = cs.screenMatchMode;
                            scaler.referencePixelsPerUnit = cs.referencePixelsPerUnit;
                        }

                        gage.gameObject.layer = c.gameObject.layer;    
                    }


                    break;
                }
                else
                {
                    canvas = target.parent.GetComponent<Canvas>();     
                }

                target = target.parent;
            }
        }
        public void AutoObject()
        {

            Transform panel = gage.transform.FindChild("Panel");
            
            if (panel == null && gage.Bar == null)
            {
                panel = CreateUIObject("Panel",gage.transform, false).transform;

                gage.name = "UIGageBar";
            }
            
            if (gage.Bar == null)
            {
                // first find
                Transform obj = gage.transform.FindChild("Bar");
                if (obj != null) gage.Bar = obj.gameObject.GetComponent<Image>();

                if (gage.Bar == null)
                {
                    GameObject go = CreateUIObject("Bar", panel);
                    gage.Bar = go.AddComponent<Image>();

                    gage.Bar.type = Image.Type.Filled;
                    gage.Bar.fillMethod = Image.FillMethod.Horizontal;
                    gage.Bar.fillOrigin = 0;
                }
            }

            if (gage.Frame == null)
            {
                // first find
                Transform obj = gage.transform.FindChild("Frame");
                if (obj != null) gage.Frame = obj.gameObject.GetComponent<Image>();

                if (gage.Frame == null)
                {
                    GameObject go = CreateUIObject("Frame", panel);
                    gage.Frame = go.AddComponent<Image>();
                }
            }
            
            if (gage.Label == null)
            {
                // first find
                Transform obj = gage.transform.FindChild("Text");
                if (obj != null) gage.Label = obj.gameObject.GetComponent<Text>();

                if (gage.Label == null)
                {
                    GameObject go = CreateUIObject("Text", panel);
                    gage.Label = go.AddComponent<Text>();
                    gage.Label.text = "Input Text";
                }
            }
        }
        

        GameObject CreateUIObject(string name, Transform parent, bool autoAnchor = true)
        {
            GameObject go = new GameObject();
            go.transform.SetParent(parent, false);
            go.name = name;
            go.layer = canvas.gameObject.layer;

            RectTransform rTransform = go.AddComponent<RectTransform>();

            if (autoAnchor)
            {
                rTransform.anchorMin = Vector2.zero;
                rTransform.anchorMax = Vector2.one;
                rTransform.anchoredPosition = Vector2.zero;
                rTransform.sizeDelta = Vector2.zero;
            }
            
            return go;
        }
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (gage.Frame == null || gage.Bar == null)
            {
                if(GUILayout.Button("Auto Object"))
                    AutoObject();
                return;
            }

//
//            gage.uiFrame = (Image)EditorGUILayout.ObjectField("Frame", gage.uiFrame, typeof(Image), true);
//            gage.uiBar = (Image)EditorGUILayout.ObjectField("Bar", gage.uiBar, typeof(Image), true);
//            gage.uiText = (Text)EditorGUILayout.ObjectField("Label", gage.uiText, typeof(Text), true);

            gage.FullTime = EditorGUILayout.FloatField("Full Time", gage.FullTime);

            gage.timerType = (UIGageBar.TimerType) EditorGUILayout.EnumPopup("Timer Type", gage.timerType);

            if( gage.Bar != null)
                gage.FillValue = EditorGUILayout.Slider("fillValue(0~1)", gage.FillValue, 0f, 1f);

            EditorGUILayout.LabelField("Value To Time", gage.ValueToTime().ToString());

            gage.Frame.sprite = (Sprite)EditorGUILayout.ObjectField("Frame", gage.Frame.sprite, typeof(Sprite), true);
            gage.Frame.color = EditorGUILayout.ColorField("Frame Color", gage.Frame.color);

            gage.Bar.sprite = (Sprite)EditorGUILayout.ObjectField("Bar", gage.Bar.sprite, typeof(Sprite), false);
            gage.Bar.color = EditorGUILayout.ColorField("Bar Color", gage.Bar.color);
            
            if (gage.Label)
            {
                gage.IsAutoText = EditorGUILayout.Toggle("Auto Text Draw", gage.IsAutoText);
                if (gage.IsAutoText)
                    gage.TextFormat = EditorGUILayout.TextField("Text format", gage.TextFormat);

                gage.Label.color = EditorGUILayout.ColorField("Text Color", gage.Label.color);
            }
            
            gage.UseBlink = EditorGUILayout.Toggle("Use Blink" ,gage.UseBlink);
            if(gage.UseBlink)
            {
                gage.blinkDrawType = (UIGageBar.BlinkDrawType)EditorGUILayout.EnumPopup("Blink Draw Type", gage.blinkDrawType);

                if( gage.blinkDrawType == UIGageBar.BlinkDrawType.Color)
                {
                    gage.BlinkColor = EditorGUILayout.ColorField("Blink Color", gage.BlinkColor);
                }
                else
                {
                    for (int i = 0; i < gage.BarSprites.Count; i++)
                    {
                        gage.BarSprites[i] = (Sprite)EditorGUILayout.ObjectField("" + i, gage.BarSprites[i], typeof(Sprite), false);
                    }

                    if (GUILayout.Button("Add Sprite"))
                    {
                        gage.BarSprites.Add(null);
                    }
                }

                gage.BlinkRatio = EditorGUILayout.FloatField("Blink Ratio", gage.BlinkRatio);
                gage.BlinkTimeGap = EditorGUILayout.FloatField("Blink Time Gap", gage.BlinkTimeGap);
            }


            //if (SceneView.lastActiveSceneView == null || SceneView.lastActiveSceneView.camera == null || sm.target == null) return;

            //Vector3 temp = SceneView.lastActiveSceneView.camera.transform.position;
            //temp.x = sm.target.position.x;            
            //SceneView.lastActiveSceneView.pivot = temp;
            //SceneView.lastActiveSceneView.Repaint();

            if ( gage.IsRun)
            {
                if (GUILayout.Button("Timer Stop"))
                {
                    gage.StopTimer();
                }
            }
            else
            {
                if (GUILayout.Button("Timer Test"))
                {
                    if( Application.isPlaying)
                    {
                        gage.StartTimer(gage.FullTime, null);
                    }
                    else
                    {
                        EditorUtility.DisplayDialog("Only You Can Test On Application Play", "Only You Can Test On Application Play", "Play");
                    }
                    
                }
            }
            

            //DrawDefaultInspector();
            
            if (GUI.changed || Application.isPlaying)
            {
                EditorUtility.SetDirty(gage);
                
                gage.Refresh();

                //serializedObject.ApplyModifiedProperties();

                if ( Application.isPlaying == false)
                // Unity Bug
                    EditorSceneManager.MarkSceneDirty(UnityEngine.SceneManagement.SceneManager.GetActiveScene());
            }
        }
    }
}