﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BzToggleTween : MonoBehaviour {
    TweenMain tween;
    GameObject activeOffTarget;

    void Awake()
    {
        if (tween == null) tween = GetComponent<TweenMain>();
    }
    void OnEnable()
    {
        if (tween != null)
        {
            //tween.ResetToBeginning();
            tween.tweenFactor = 0;
        }
    }
    void OnDisable()
    {
        tween.OnFinished.RemoveListener(OnFinish);
    }
    public void Reverse()
    {
        Reverse(null);
    }
    public void Reverse(GameObject _activeOffTarget)
    {
        if (tween != null)
        {
            tween.PlayReverse();
            if (_activeOffTarget)
            {
                activeOffTarget = _activeOffTarget;
            }
            tween.OnFinished.AddListener(OnFinish);
        }
    }
    public void Toggle(bool value)
    {
        if (value) tween.Play();
        else Reverse();
    }

    void OnFinish()
    {
        if (activeOffTarget)
            activeOffTarget.SetActive(false);
    }
}
