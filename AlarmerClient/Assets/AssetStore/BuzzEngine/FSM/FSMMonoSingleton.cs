using System;
using UnityEngine;

namespace BuzzEngine.FSM
{
    public class FSMMonoSingleton<T> : MonoBehaviour where T : FSMMonoSingleton<T>
    {
        #region FSM Copy From FSM<T>

        public string StateName = "Null";

        /// <summary>
        ///     current State
        /// </summary>
        public FSMState<T> State { get; private set; }

        /// <summary>
        ///     직전 스테이트
        /// </summary>
        private FSMState<T> _previousState;

        public void FSMUpdate()
        {
            if (State != null) State.Execute();
        }

        public void SetState(T stateTarget, FSMState<T> newState)
        {
            _previousState = State;

            if (_previousState != null)
                _previousState.Exit();

            if (newState != null)
            {
                State = newState;
                newState.Target = stateTarget;

                var str = newState.ToString();
                var startIndex = str.LastIndexOf(".", StringComparison.Ordinal) + 1;
                str = str.Substring(startIndex);
                StateName = str;

                newState.Enter();
            }
        }

        public void RevertState(T stateTarget)
        {
            if (_previousState != null)
                SetState(stateTarget, _previousState);
        }

        public void NextState()
        {
            if (State != null)
                State.Next();
        }

        #endregion
        #region Singleton

        /// <summary>
        ///     Instance
        /// </summary>
        private static T _staticInstance;

        //instance
        public static T I
        {
            get
            {
                // Instance requiered for the first time, we look for it
                if (_staticInstance != null) return _staticInstance;

                _staticInstance = FindObjectOfType(typeof(T)) as T;

                // Object not found, we create a temporary one
                if ((_staticInstance == null) && Application.isPlaying)
                {
                    //Debug.LogWarning("No instance of " + typeof(T).ToString() + ", a temporary one is created.");
                    _staticInstance = new GameObject(typeof(T).ToString(), typeof(T)).GetComponent<T>();

                    var parent = GameObject.Find("SingleTons") ?? new GameObject("SingleTons");

                    _staticInstance.transform.parent = parent.transform;

                    // Problem during the creation, this should not happen
                    if (_staticInstance == null)
                        Debug.LogError("Problem during the creation of " + typeof(T));
                }
                if (_staticInstance != null) _staticInstance.Init();
                return _staticInstance;
            }
        }

        //// If no other monobehaviour request the instance in an awake function
        //// executing before this one, no need to search the object.
        //private void Awake()
        //{
        //    if( _staticInstance == null )
        //    {
        //        _staticInstance = this as T;
        //        _staticInstance.Init();
        //    }
        //}

        // This function is called when the instance is used the first time
        // Put all the initializations you need here, as you would do in Awake
        public virtual void Init()
        {
        }

        // Make sure the instance isn't referenced anymore when the user quit, just in case.
        public void OnApplicationQuit()
        {
            _staticInstance = null;
        }

        public virtual void OnPause()
        {
        }

        #endregion
    }
}