using System;
using System.CodeDom;
using System.Collections;
using UnityEngine;

namespace BuzzEngine.FSM
{
    /// <summary>
    ///     하나의 단일 생명체(오브젝트)를 지칭하는 클래스
    /// </summary>
    public abstract class FSMMono<T> : MonoBehaviour where T : FSMMono<T>
    {
        public string StateName; 
           
        public FSMMonoState<T> State { get; private set; }
        /// <summary>
        /// 상태를 변경한다.
        /// </summary>
        /// <param name="stateTarget"></param>
        /// <param name="e"></param>
        public void ChangeState(T stateTarget,Enum e)
        {
            ChangeState(stateTarget, e.ToString());
        }
        public void ChangeState(T stateTarget, string className)
        {
            var nspace = typeof(T).Namespace;

            nspace = string.IsNullOrEmpty(nspace) ? "" : nspace + ".";

            ChangeState(stateTarget, Type.GetType(nspace + className));
        }
        public void ChangeState(T stateTarget, Type type)
        {
            if ((State != null) && (type == State.GetType())) return;

            if (State != null)
            {
                _preState = State;
                State.Exit();
            }

            State = gameObject.AddComponent(type) as FSMMonoState<T>;

            if (State != null)
            {
                State.Mono = stateTarget;
                StateName = State.ToString();
                var si = StateName.LastIndexOf(".", StringComparison.Ordinal) + 1;
                
                StateName = StateName.Substring(si).Replace(")","");
            }
        }

        public void ChangeState(T stateTarget, FSMMonoState<T> state)
        {
            if ((State != null) && (State == state)) return;
            
            State = state;

            if (State != null)
            {
                State.Mono = stateTarget;
                StateName = State.ToString();
                var si = StateName.LastIndexOf(".", StringComparison.Ordinal) + 1;

                StateName = StateName.Substring(si).Replace(")", "");
            }
        }
        public bool EqualState(Enum e)
        {
            return EqualState(e.ToString());
        }
        public bool EqualState(string type)
        {
            return EqualState(Type.GetType(type));
        }
        public bool EqualState(Type type)
        {
            return (State != null) && (type == State.GetType());
        }

        private FSMMonoState<T> _preState;
        public void RevertState(T stateTarget)
        {
            ChangeState(stateTarget, _preState.GetType());
        }
    }

    public abstract class FSMMonoState<T> : MonoBehaviour where T : FSMMono<T>
    {
        public T Mono { get; set; }

        public virtual IEnumerator Initialize()
        {
            yield return null;
        }

        public void Exit()
        {
            if (Mono.State != null)
            {
                DestroyObject(this);
            }
        }
        public void ChangeState(Enum e)
        {
            Mono.ChangeState(Mono,e);
        }
        public void ChangeState(string className)
        {
            Mono.ChangeState(Mono,className);
        }
        public void ChangeState(Type type)
        {
            Mono.ChangeState(Mono,type);
        }

        public void ChangeState(FSMMonoState<T> state)
        {
            Mono.ChangeState(Mono,state);
        }
        public bool EqualState(Enum e)
        {
            return Mono.EqualState(e);
        }
        public bool EqualState(string type)
        {
            return Mono.EqualState(type);
        }
        public bool EqualState(Type t)
        {
            return Mono.EqualState(t);
        }
        public void RevertState()
        {
            Mono.RevertState(Mono);
        }
        //public virtual void Enter(BuzzEngine.XGameObjectMonobehaviour.FSMMono xGameObject) { }
    }
}