//-----------------------------------------------------------------------
// <copyright file="FSM.cs" company="CompanyName">
//     Company copyright tag.
// FSM<T>, FSM, FSM{T}
// </copyright>
//-----------------------------------------------------------------------
//--------------------------------------------------------
/* Copyright (c) Buzzment. All rights reserved.
 * author       : Heyya
 * date         : 11/08/2013 ~ 11/08/2016
 */

using System;
using System.Collections;
using UnityEngine;

namespace BuzzEngine.FSM
{
    /// <summary>
    /// FSM
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FSM<T>
    {
        /// <summary>
        ///     대상의 스테이트를 초기화 한다.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="initialState"></param>
        public void Initialize(T stateTarget,FSMState<T> initialState)
        {
            SetState(stateTarget,initialState);
        }
        
        public string StateName = "Null";

        /// <summary>
        ///     current State
        /// </summary>
        public FSMState<T> State { get; private set; }

        /// <summary>
        ///     직전 스테이트
        /// </summary>
        private FSMState<T> _previousState;

        void Update()
        {
            Debug.Log("HI");
        }
        public void FSMUpdate()
        {
            if (State != null) State.Execute();
        }
        
        public void SetState(T stateTarget, FSMState<T> newState)
        {
            _previousState = State;

            if (_previousState != null)
                _previousState.Exit();

            if (newState != null)
            {
                State = newState;
                newState.Target = stateTarget;

                var str = newState.ToString();
                var startIndex = str.LastIndexOf(".", StringComparison.Ordinal) + 1;
                str = str.Substring(startIndex);
                StateName = str;
                
                newState.Enter();
            }
        }

        public void RevertState(T stateTarget)
        {
            if (_previousState != null)
                SetState(stateTarget, _previousState);
        }

        public void NextState()
        {
            if (State != null)
                State.Next();
        }
    }

    public abstract class FSMState<T>
    {
        public T Target;

        public abstract void Enter();

        public abstract void Execute();

        public abstract void Exit();

        public virtual void Next()
        {

        }

        public virtual IEnumerator Initialize()
        {
            yield return null;
        }
    }
}