﻿using System;
using System.Collections;
using System.Collections.Generic;
using BuzzEngine.Generic;
using JsonFx.Json;
using UnityEngine;
using Htp = BuzzEngine.Generic.BzHttpConnection;

namespace BuzzEngine.Samples
{
    public class HttpConnection : BzMonoSingleton<HttpConnection>
    {
        #region Protocols
        public class ProtocolClass
        {
            public int Score = 0;
            public DateTime UpdateDate;
        }
        #endregion
        #region Sample

        public enum AD
        {
            Main,Host,Service,Ranking
        }

        public Dictionary<Enum, string> Address = new Dictionary<Enum, string>();

        public override void Init()
        {
            Address.Add(AD.Main,"http://localhost");
        }

        public Coroutine RequestMultiArray(string patientSeq, string gameCode, Action<ProtocolClass[]> data)
        {
            return StartCoroutine(iRequestMultiArray(patientSeq, gameCode, data));
        }

        private IEnumerator iRequestMultiArray(string patientSeq, string gameCode, Action<ProtocolClass[]> data)
        {
            var url = Address[AD.Main] + "/rm3/proc/client/game_rank.php";

            var postData = new Dictionary<string, string>
            {
                {"type", "S"},
                {"PatientSeq", patientSeq},
                {"Code", gameCode}
            };

            yield return Htp.i.Request<ProtocolClass[]>(url, postData, Htp.ePostType.Sync, data);
        }
        
        public Coroutine RequestSingle(string patientSeq, string gameCode, string score, Action<int> data)
        {
            return StartCoroutine(iRequestSingle(patientSeq, gameCode, score, data));
        }
        private IEnumerator iRequestSingle(string patientSeq, string gameCode, string score, Action<int> data)
        {
            var url = Address[AD.Main] + "/rm3/proc/client/game_rank.php";

            var postData = new Dictionary<string, string>
            {
                {"type", "R"},
                {"PatientSeq", patientSeq},
                {"Code", gameCode},
                {"Score", score}
            };
            yield return Htp.i.Request<int>(url, postData, Htp.ePostType.Sync, data);
        }

        private IEnumerator iSendSingle(string data, string protocolResultSeq)
        {
            var url = Address[AD.Main] + "/rm3/proc/client/game_result.php";

            var postData = new Dictionary<string, string>
            {
                {"type", "I"},
                {"GameResult", data},
                {"ResultGroup", protocolResultSeq}
            };
            // game option json data

            yield return Htp.i.Request<string>(url, postData, Htp.ePostType.Sync, null);
        }

        public Coroutine SendList(ProtocolClass[] resultList)
        {
            return StartCoroutine(iSendList(resultList));
        }

        private IEnumerator iSendList(ProtocolClass[] resultList)
        {
#if UNITY_EDITOR || GAMELOG
            var strLog = "Request.iSendGameProtocalResult\n";
            strLog += "URL: WebServer.Function\n";
            Debug.Log(BzLog.WrapColor(strLog, eLogCat.Net));
#endif
            var url = Address[AD.Main] + "/rm3/proc/client/protocol_result_report.php";
            
            var postData = new Dictionary<string, string>();

            var builder = new System.Text.StringBuilder();
            builder.Append(JsonWriter.Serialize(resultList));

            postData.Add("type", "I");
            postData.Add("data", builder.ToString());

            yield return Htp.i.Request<string>(url, postData, Htp.ePostType.Sync, null);
        }
        #endregion
    }
}
