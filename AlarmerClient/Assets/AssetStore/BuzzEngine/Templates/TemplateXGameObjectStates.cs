﻿using UnityEngine;

namespace BuzzEngine.Templates //Change Project Namespace
{
    public abstract class TemplateXGameObjectStates : BuzzEngine.XGameObject.XGameObjectState // ## Change Template to GameName ##
    {
        public TemplateXGameObject Main = null;

        public void ConvertXGameObject(BuzzEngine.XGameObject.XGameObject xObj)
        {
            Main = (TemplateXGameObject) xObj;
        }
    }

    public class TemplateXGameObjectStateIdle : TemplateXGameObjectStates
    {
        public override void Enter(BuzzEngine.XGameObject.XGameObject xObj)
        {
            ConvertXGameObject(xObj);
        }

        public override void Update()
        {

        }

        public override void OnCollisionEnter2D(Collision2D collision)
        {

        }

        public override void OnTriggerExit2D(Collider2D collision)
        {
        }
    }
}