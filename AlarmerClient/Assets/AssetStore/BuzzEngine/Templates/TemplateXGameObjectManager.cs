﻿using System.Collections;

namespace BuzzEngine.Templates //Change Project Namespace
{
    public class TemplateXGameObjectManager : BuzzEngine.XGameObject.XGameObjectManager // Change Template to GameName
    {
        #region State & Species

        /// <summary>
        /// 게임내 캐릭터의 동작을 정의 한다.
        /// </summary>
        public enum EnState
        {
            None = -1,
            Idle = 0
        }

        public enum EnSpecies
        {
            Normal,
            Count,
        }
        /// <summary>
        /// return EnSpecis Count
        /// </summary>
        /// <returns></returns>
        public override int GetSpeciesCount()
        {
            return (int) EnSpecies.Count;
        }
        /// <summary>
        /// return to string type Species from index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public override string GetSpeciesFromIndex(int index)
        {
            return ((EnSpecies) index).ToString();
        }

        #endregion
#pragma warning disable 0414
        TemplateXGameObject _pObjPrefab = null;
#pragma warning restore 0414

        public TemplateXGameObjectManager(TemplateXGameObject pObjPrefab)
        {
            _pObjPrefab = pObjPrefab;
        }


        public override IEnumerator Initialize()
        {
            //if (pObjPrefab == null)
            //{
            //    yield return RMGResource.i.Load<TemplateXGameObject>(RMG.GameTitles.AAplication, RMGResource.ePath.PlayObjects, "kingkong", v => pObjPrefab = v);
            //    if (pObjPrefab == null) { Debug.LogError("Not found resource : " + "kingkong"); yield break; }
            //}
            yield break;
        }

        protected override IEnumerator iSpawnToXGameObject(BuzzEngine.XGameObject.XSpawn spawn)
        {
            //Transform spawntf = spawn.transform;
            //TemplateXGameObject box = Instantiate(pObjPrefab, spawntf.position, spawntf.rotation) as TemplateXGameObject;
            //box.transform.parent = transform;
            //box.spawn = spawn;

            yield return null;
        }
    }

}