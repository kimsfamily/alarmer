﻿using UnityEngine;
using BuzzEngine;
using System.Collections;
using eState = BuzzEngine.Templates.TemplateFSMMono.eState;

namespace BuzzEngine.Templates
{
    public abstract class TemplateFSMMonoState : BuzzEngine.FSM.FSMMonoState<TemplateFSMMono>
    {
        public override IEnumerator Initialize()
        {
            return base.Initialize();
        }
    }

    public class Load : TemplateFSMMonoState
    {
        IEnumerator Start()
        {
            ChangeState(eState.Idle);
            return base.Initialize();
        }
    }

    public class Idle : TemplateFSMMonoState
    {
        void Start()
        {
            ChangeState(eState.IloveYou);
        }
    }

    public class IloveYou : TemplateFSMMonoState
    {
        void Start()
        {
            Debug.Log(this);
        }
    }
}