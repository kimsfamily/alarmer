﻿//using UnityEngine;
//using System.Collections;

namespace BuzzEngine.Templates // ## Change Project Namespace ##
{
    using EnState = TemplateXGameObjectManager.EnState;
    using EnSpecies = TemplateXGameObjectManager.EnSpecies;

    public class TemplateXGameObject : BuzzEngine.XGameObject.XGameObject // ## Change Template to GameName ##
    {
        /// <summary>
        /// 활성화시 초기 스테이트를 결정한다.
        /// </summary>
        public EnState StartState = EnState.None;

        public EnSpecies Species = EnSpecies.Count;

        public override void Awake()
        {
            AddStateDic(EnState.Idle, new TemplateXGameObjectStateIdle());
            // 모든 스테이트를 등록 후 반드시 각 스테이트에대한 초기화를 해줘야 한다.
            InitStates(this);
            // 처음 실행 시킬 상탱 등록(인스펙터에서 정의 할수도 있고 소스로 정의 할수도 있다.
            StartState = EnState.Idle;
        }

        public override void OnEnable()
        {
            //if (controler == null) controler = Hierarchy.i.Manager;
            if (controler != null) controler.RegistXGameObject(Species.ToString(), this);

            SetCurrentState(StartState);
        }

        public override void OnDisable()
        {
            //		bDie = false;
            if (controler != null) controler.UnRegistXGameObject(this);
            if (spawn != null) spawn.UnRegistXGameObject(this);

            StopAllCoroutines();
        }
    }
}