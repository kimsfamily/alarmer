﻿using UnityEngine;
using BuzzEngine;
using System.Collections;

namespace BuzzEngine.Templates
{
    public class TemplateFSMMono : BuzzEngine.FSM.FSMMono<TemplateFSMMono>
    {
        public enum eState
        {
            Load,Idle,IloveYou
        }

        public eState StartState;
        
        IEnumerator Start()
        {
            yield return null;
            ChangeState(this, StartState);
        }

        void Update()
        {
            
        }
    }
}