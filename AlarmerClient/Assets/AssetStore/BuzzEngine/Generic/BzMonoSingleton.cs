using UnityEngine;

namespace BuzzEngine.Generic
{
    public abstract class BzMonoSingleton<T> : MonoBehaviour where T : BzMonoSingleton<T>
    {
        public static T Instance = null;
        //instance
        public static T i
        {
            get
            {
                // Instance requiered for the first time, we look for it
                if( Instance == null )
                {
                    Instance = GameObject.FindObjectOfType(typeof(T)) as T;

                    // Object not found, we create a temporary one
                    if( Instance == null && Application.isPlaying)
                    {
                        //Debug.LogWarning("No instance of " + typeof(T).ToString() + ", a temporary one is created.");
                        Instance = new GameObject(typeof(T).ToString(), typeof(T)).GetComponent<T>();

                        var parent = GameObject.Find("SingleTons");
                        if (parent == null) parent = new GameObject("SingleTons");

                        Instance.transform.parent = parent.transform;

                        // Problem during the creation, this should not happen
                        if( Instance == null )
                        {
                            Debug.LogError("Problem during the creation of " + typeof(T).ToString());
                        }
                    }
                    Instance.Init();
                }
                return Instance;
            }
        }
        //// If no other monobehaviour request the instance in an awake function
        //// executing before this one, no need to search the object.
        //private void Awake()
        //{
        //    if( m_Instance == null )
        //    {
        //        m_Instance = this as T;
        //        m_Instance.Init();
        //    }
        //}

        // This function is called when the instance is used the first time
        // Put all the initializations you need here, as you would do in Awake
        public virtual void Init(){}

        // Make sure the instance isn't referenced anymore when the user quit, just in case.
        private void OnApplicationQuit()
        {
            Instance = null;
        }
        public virtual void OnPause(){
		
        }
    }
}