﻿using UnityEngine;
using System.Collections;
using BuzzEngine;

namespace BuzzEngine
{
    public enum eLogColor
    {
        white,
        black,
        blue,
        cyan,
        gray,
        green,
        magenta,
        red,
        yellow,
    }

    public enum eLogCat
    {
        Normal,
        Notice,
        Build,
        Net,
        File,
        Data,
        Memory,
        Flow,
    }

    public static class BzLog
    {
        public static string WrapColor(string msg, eLogCat cat = eLogCat.Normal)
        {
            switch (cat)
            {
                case eLogCat.Normal:msg = WrapColor(msg, "gray"); break;
                case eLogCat.Notice:msg = WrapColor("[Notice]", "white") + msg; break;
                case eLogCat.Build: msg = WrapColor("[Build]", "yellow") + msg; break;
                case eLogCat.Net:   msg = WrapColor("[Net]", "#99FF99") + msg; break;
                case eLogCat.File:  msg = WrapColor("[File]", "#red") + msg; break;
                case eLogCat.Data:  msg = WrapColor("[Data]", "#magenta") + msg; break;
                case eLogCat.Memory:msg = WrapColor("[Memory]", "#blue") + msg; break;
                case eLogCat.Flow:  msg = WrapColor("[Flow]", "#cyan") + msg; break;
            }
            return msg;
        }

        public static string WrapColor(string msg, eLogColor color = eLogColor.white)
        {
            return WrapColor(msg, color.ToString());
        }

        // eColor 대신 UnityEngine.Color 또한 사용 할수 있게 ex) LogC("hi", Color.white);
        // 256 Color 만 지원 하는 관계로 color 대입시 원치 않는 컬러가 나올수 있다. 
        // 잡기술 인관계로 패스~~~~
        public static string WrapColor(string msg, Color color)
        {
            string colorCode = "#";
            colorCode += string.Format("{0:X2}", (System.Int32)(color.r * 255));
            colorCode += string.Format("{0:X2}", (System.Int32)(color.g * 255));
            colorCode += string.Format("{0:X2}", (System.Int32)(color.b * 255));
            return WrapColor(msg, colorCode);
        }

        // only support 256 Color Code
        // sample #ffffff,#??????, white, black, blue, cyan, green, magenta, red, yellow
        public static string WrapColor(string msg, string colorCode256)
        {
            return string.Format("<color=" + colorCode256 + ">{0}</color>", msg);
        }
    }
}
