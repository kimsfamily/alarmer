﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using JsonFx.Json;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace BuzzEngine.Generic
{
    public class BzHttpConnection : MonoBehaviour
    {
        #region Singleton
        public static BzHttpConnection Instance = null;
        //instance
        public static BzHttpConnection i
        {
            get
            {
                // Instance requiered for the first time, we look for it
                if (Instance == null)
                {
                    Instance = GameObject.FindObjectOfType(typeof(BzHttpConnection)) as BzHttpConnection;

                    // Object not found, we create a temporary one
                    if (Instance == null && Application.isPlaying)
                    {
                        Instance = new GameObject(typeof(BzHttpConnection).ToString(), typeof(BzHttpConnection)).GetComponent<BzHttpConnection>();

                        var parent = GameObject.Find("SingleTons");
                        if (parent == null) parent = new GameObject("SingleTons");

                        Instance.transform.parent = parent.transform;

                        // Problem during the creation, this should not happen
                        if (Instance == null)
                        {
                            Debug.LogError("Problem during the creation of " + typeof(BzHttpConnection).ToString());
                        }
                    }
                    Instance.Init();
                }
                return Instance;
            }
        }

        // This function is called when the instance is used the first time
        // Put all the initializations you need here, as you would do in Awake
        public virtual void Init() { }

        #endregion
        
        public enum ePostType
        {
            Sync,
            ASync,
        }

        public WWW PostDataSync(string url, string postData)
        {
            return PostData(true, url, postData);
        }
        public WWW PostDataASync(string url, string postData)
        {
            return PostData(false, url, postData);
        }

        WWW PostData(bool bSync, string url, string postData)
        {

            postData = postData.ToString().Replace("k__BackingField", "");
            postData = postData.ToString().Replace("<", "");
            postData = postData.ToString().Replace(">", "");
            postData = postData.ToString().Replace("\r\n", "");

            var encoding = new System.Text.UTF8Encoding();
            var postHeader = new Dictionary<string, string>();
            postHeader.Add("Content-Type", @"application/x-www-form-urlencoded");
            postHeader.Add("Content-Length", postData.Length.ToString());

            var www = new WWW(url, encoding.GetBytes(postData), postHeader);
            if (bSync)
                while (!www.isDone) { }

            return www;
        }

        protected string PostDataDicToString(Dictionary<string, string> data, bool isAes = true)
        {
            var rtVal = "";
            foreach (var s in data)
            {
                // 처음 변수 외에는 &를 붙여 준다.
                if (rtVal != "") rtVal += "&";

                if ("type".Equals(s.Key) || isAes == false)
                    rtVal += s.Key + "=" + s.Value;
                else // 위 조건 외에는 암호를 진행 한다.
                    rtVal += s.Key + "=" + (string.IsNullOrEmpty(s.Value) ? "" : Wenect.Security.AES.WebEnc(s.Value));
            }

            return rtVal;
        }

        protected void ConvertFromJson<T>(string strData, Action<T> data)
        {
            var type = typeof(T);

            if (type == typeof(int) || type == typeof(float) || type == typeof(double) || type == typeof(string) ||
                type == typeof(bool))
            {
                var sid = strData.IndexOf(":", StringComparison.Ordinal) + 1;
                var eid = strData.LastIndexOf("}", StringComparison.Ordinal) - 1;

                var temp = strData;
                if (eid > 0)
                {
                    temp = temp.Substring(sid, eid - sid);
                    temp = temp.Replace("\"", "");
                    temp = temp.Replace("'", "");
                }

                try
                {
                    var mem = (T) Convert.ChangeType(temp, typeof(T));

                    data(mem);
                }
                catch (Exception e)
                {
                    Debug.LogError("Net Data Convert Error : " + temp + " error : " + e);
                }
            }
            else
            {
                try
                {
                    data(JsonReader.Deserialize<T>(strData));
                }
                catch (Exception e)
                {
                    Debug.LogError("Json Mapping Error : " + strData + " error : " + e);
                }
            }
        }

        public Coroutine Request<T>(string url, string postData, ePostType postType, Action<T> data)
        {
            return StartCoroutine(iRequest(url, postData, postType, data, new StackTrace().GetFrame(1).GetMethod().Name));
        }

        public Coroutine Request<T>(string url, string postData, ePostType postType, Action<T> data, string ptCode)
        {
            return StartCoroutine(iRequest(url, postData, postType, data, ptCode));
        }

        public Coroutine Request<T>(string url, Dictionary<string, string> postData, ePostType postType, Action<T> data)
        {
            return StartCoroutine(iRequest(url, postData, postType, data, new StackTrace().GetFrame(1).GetMethod().Name));
        }

        public Coroutine Request<T>(string url, Dictionary<string, string> postData, ePostType postType, Action<T> data, string ptCode)
        {
            return StartCoroutine(iRequest(url, postData, postType, data, ptCode));
        }

        protected IEnumerator iRequest<T>(string url, string postData, ePostType postType, Action<T> data, string ptCode)
        {
            var www = postType == ePostType.Sync ? PostDataSync(url, postData) : PostDataASync(url, postData);
            
            if (null != www.error)
            {
                Debug.LogError(BzLog.WrapColor(ptCode + " Error : www.error", eLogCat.Net));
                yield break;
            }

            var strData = www.text;

            if (strData == null || strData == "null")
            {
                Debug.LogError(BzLog.WrapColor(ptCode + " Error : www.text is null", eLogCat.Net));
                yield break;
            }

            strData = Wenect.Security.AES.WebDec(strData);

            if (strData == "N")
            {
                Debug.LogError(
                    BzLog.WrapColor(ptCode + " Error : return error code : " + strData, eLogCat.Net));
                yield break;
            }

            if (data == null) yield break;
            // 처리가 완벽 하다면 
            ConvertFromJson<T>(strData, data);

#if UNITY_EDITOR || GAMELOG
            var strLog = "Response." + ptCode + "\n";
            strLog += "Type : " + data + "\n";
            strLog += "Value : " + strData + "\n";
            Debug.Log(BzLog.WrapColor(strLog.Replace("},{", "},\n{").Replace("[{", "[\n{"), eLogCat.Net));
#endif
        }

        protected IEnumerator iRequest<T>(string url, Dictionary<string, string> postData, ePostType postType,Action<T> data, string ptCode)
        {
#if UNITY_EDITOR || GAMELOG
            //string strLog = "Request." + System.Reflection.MethodBase.GetCurrentMethod().Name + "\n";
            var strLog = "Request." + ptCode + "\n";
            strLog += "URL:" + url + "\n";
            strLog += "POST:" + PostDataDicToString(postData, false) + "\n";
            Debug.Log(BzLog.WrapColor(strLog, eLogCat.Net));
#endif

            var aesData = PostDataDicToString(postData);

            yield return StartCoroutine(iRequest<T>(url, aesData, postType, data, ptCode));
        }

    }
}