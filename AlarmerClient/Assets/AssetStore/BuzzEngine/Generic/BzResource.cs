﻿using System.Collections;
using UnityEngine;

namespace BuzzEngine.Generic
{
    public class BzResource : BzMonoSingleton<BzResource>
    {
        public bool UseLog = false;
        public Coroutine Load<T>(string path, System.Action<T> result)
        {
            return StartCoroutine(iLoad<T>(path, result));
        }
        IEnumerator iLoad<T>(string path, System.Action<T> result)
        {
            var mem = (T)System.Convert.ChangeType(Resources.Load(path, typeof(T)), typeof(T));

            if (mem == null)
            {
                Debug.LogError("Not found source : (" + typeof(T) + ")" + path);
                yield break;
            }
            else if (UseLog)
            {
                Debug.Log(BuzzEngine.BzLog.WrapColor("Sucess resource load : (" + typeof(T) + ")" + path, Color.green));
            }

            yield return new WaitForFixedUpdate();

            result(mem);
        }
        public Coroutine LoadBundle<T>(string path, System.Action<T> result)
        {
            return StartCoroutine(iLoadBundle<T>(path, result));
        }
        IEnumerator iLoadBundle<T>(string path, System.Action<T> result)
        {
            // Start a download of the given URL
            var www = WWW.LoadFromCacheOrDownload(path, 1);

            // Wait for download to complete
            yield return www;

            // Load and retrieve the AssetBundle
            var bundle = www.assetBundle;

            // Load the object asynchronously
            var request = bundle.LoadAssetAsync("actionguids.g1", typeof(T));

            // Wait for completion
            yield return request;

            // Get the reference to the loaded object
            var mem = (T)System.Convert.ChangeType(request.asset, typeof(T));

            // Unload the AssetBundles compressed contents to conserve memory
            bundle.Unload(false);

            // Frees the memory from the web stream
            www.Dispose();

            if (mem == null)
            {
                Debug.LogError("Not found bundle source : (" + typeof(T) + ")" + path);
                yield break;
            }
            else if (UseLog)
            {
                Debug.Log(BuzzEngine.BzLog.WrapColor("Sucess resource load : (" + typeof(T) + ")" + path, Color.green));
            }

            result(mem);
        }
        public Coroutine Create<T>(string path, System.Action<T> result, Transform _parent = null, bool _worldPositionStays = false)
        {
            return StartCoroutine(iCreate(path, result, _parent, _worldPositionStays));
        }
        IEnumerator iCreate<T>(string path, System.Action<T> result, Transform _parent = null, bool _worldPositionStays = false)
        {
            GameObject mem = null;

            yield return iLoad<GameObject>(path, m => mem = m);

            if (mem == null) yield break;

            mem = GameObject.Instantiate(mem) as GameObject;

            if (UseLog)
            {
                Debug.Log(BuzzEngine.BzLog.WrapColor("Sucess Create : (" + typeof(T) + ")" + path, Color.green));
            }

            mem.transform.SetParent(_parent, _worldPositionStays);

            var target = mem.GetComponent<T>();

            result(target);
        }


        #region For Bundle
        //public Coroutine LoadBundle<T>(string assetBundleName, string assetName,System.Action<T> result) where T : UnityEngine.Object
        //{
        //    return StartCoroutine(iLoadBundle<T>(assetBundleName, assetName, result));
        //}
        //IEnumerator iLoadBundle<T>(string assetBundleName, string assetName, System.Action<T> result) where T : UnityEngine.Object
        //{
        //    // Load asset from assetBundle.
        //    AssetBundleLoadAssetOperation request = AssetBundleManager.LoadAssetAsync(assetBundleName, assetName, typeof(GameObject));
        //    if (request == null)
        //    {
        //        Debug.LogError("Failed AssetBundleLoadAssetOperation on " + assetName + " from the AssetBundle " + assetBundleName + ".");
        //        yield break;
        //    }

        //    yield return StartCoroutine(request);

        //    if(request.GetAsset<T>().GetType().Equals(typeof(T)) == false)
        //    {
        //        Debug.LogError("Request Type(" + request.GetType() + ") is not equal BundleType(" + typeof(T) + ")");
        //    }

        //    T mem = request.GetAsset<T>();

        //    result(mem);
        //}
        //public Coroutine LoadBundle(string assetBundleName, string assetName, GameObject result)
        //{
        //    //return StartCoroutine(iLoadBundle<T>(path, result));
        //    return StartCoroutine(iLoadBundle(assetBundleName, assetName,result));
        //}
        //protected IEnumerator iLoadBundle(string assetBundleName, string assetName, GameObject result)
        //{
        //    // Load asset from assetBundle.
        //    AssetBundleLoadAssetOperation request = AssetBundleManager.LoadAssetAsync(assetBundleName, assetName, typeof(GameObject));
        //    if (request == null)
        //    {
        //        Debug.LogError("Failed AssetBundleLoadAssetOperation on " + assetName + " from the AssetBundle " + assetBundleName + ".");
        //        yield break;
        //    }

        //    yield return StartCoroutine(request);

        //    result = request.GetAsset<GameObject>();
        //}
        //        IEnumerator iLoadBundle<T>(string path, System.Action<T> result)
        //        {
        //            // Start a download of the given URL
        //            WWW www = WWW.LoadFromCacheOrDownload(path, 1);

        //            // Wait for download to complete
        //            yield return www;

        //            // Load and retrieve the AssetBundle
        //            AssetBundle bundle = www.assetBundle;

        //            // Load the object asynchronously
        //            AssetBundleRequest request = bundle.LoadAssetAsync("actionguids.g1", typeof(T));

        //            // Wait for completion
        //            yield return request;

        //            // Get the reference to the loaded object
        //            T mem = (T)System.Convert.ChangeType(request.asset, typeof(T));

        //            // Unload the AssetBundles compressed contents to conserve memory
        //            bundle.Unload(false);

        //            // Frees the memory from the web stream
        //            www.Dispose();

        //            if (mem == null)
        //            {
        //#if GAMELOG
        //            Debug.LogError("Not found bundle source : (" + typeof(T) + ")" + path);
        //#endif
        //                yield break;
        //            }
        //            else
        //            {
        //#if GAMELOG
        //            Debug.Log(Buzz.BzLog.WrapColor("Sucess resource Bundle load : (" + typeof(T) + ")" + path, Color.green));
        //#endif
        //            }
        //            result(mem);
        //        }
        #endregion
    }
}