﻿using System;
using System.IO;
using BuzzEngine.Editor.Widget;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BuzzEngine.Editor
{
    public class ScriptMaker : BzEditor
    {
        public const string MENU_ITEM = "Assets/Buzz/Script";
        public const string BUZZ_ENGINE_ROOT = "AssetStore/BuzzEngine";
        public const string TEMPLATE_PATH = BUZZ_ENGINE_ROOT + "/Templates/";
        /// <summary>
        /// 변경할 소스 (해당 문자를 찾아서 tar로 변경 한다.
        /// </summary>
        public const string REPLACE_STRING = "Template";
        /// <summary>
        /// 변경할 네임 스페이스(해당 네임스페이스를 찾아 변경한다.)
        /// </summary>
        public const string REPLACE_NAMESPACE = "BuzzEngine.Templates";

        [MenuItem(MENU_ITEM + "/Singleton")]
        [MenuItem(MENU_ITEM + "/FSM")]
        [MenuItem(MENU_ITEM + "/BzObjectCollector")]
        public static void Sample()
        {
            
        }

        public static string Addd()
        {
            return "dkdk";
        }
        /// <summary>
        /// 해당 경로에 디렉토리 경로를 리턴 한다.
        /// 파일인 경우 상위 폴더를 리턴한다.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetDirectoryAssetPath(Object target)
        {
            var info = new DirectoryInfo(AssetDatabase.GetAssetPath(target));
            
            DirectoryInfo rootDir = null;

            if (info.Parent != null) rootDir = info.Exists ? info : info.Parent;

            System.Diagnostics.Debug.Assert(rootDir != null, "rootDir != null");

            return GetAssetPath(rootDir.FullName);
        }

        public static string GetAssetPath(string path)
        {
            var startid = path.IndexOf("Assets", StringComparison.Ordinal);

            return path.Substring(startid);
        }
        
        public static string GetSourceFromTemplete(string templetePath)
        {
            var folderPath = templetePath + ".cs";

            var copyPath = Application.dataPath + "/" + folderPath;

            var rtVal = "";
            if (File.Exists(copyPath) == true)
            {
                using (var outfile = new StreamReader(copyPath)) { rtVal = outfile.ReadToEnd(); }
            }
            else
            {
                Debug.LogError("Is Not Exist templete file " + copyPath);
            }

            return rtVal;
        }
        
        public static void CreateDefaultCSFile(string filePath, string source)
        {
            filePath += ".cs";

            if (File.Exists(filePath))
            {
                // do not overwrite
                if (EditorUtility.DisplayDialog("Warnning", filePath + " File이 이미 존재 합니다.\n새로 쓰시겠습니까?", "OK", "NO"))
                {
                    using (var outfile = new StreamWriter(filePath))
                    {
                        outfile.WriteLine(source);
                    }
                }
            }
            else
            {
                using (var outfile = new StreamWriter(filePath)) { outfile.WriteLine(source); }
            }
        }

        public static void CreateCscriptFiles(string tempClassName, string tarClassName = null)
        {
            var tar = Selection.activeObject.name;

            var savePath = GetDirectoryAssetPath(Selection.activeObject);

            var namespaceTar = savePath.Replace("Assets\\", "").Replace("\\", ".");
            
            var temp = GetSourceFromTemplete(TEMPLATE_PATH + "Template" + tempClassName);
            if (tarClassName == null)
            {
                tarClassName = tempClassName;
            }
            else if(tarClassName.Equals(""))
            {
                temp = temp.Replace(REPLACE_STRING + tempClassName, tar);
            }
            CreateDefaultCSFile(savePath + "/" + tar + tarClassName, temp.Replace(REPLACE_NAMESPACE, namespaceTar).Replace(REPLACE_STRING, tar));
            
            AssetDatabase.Refresh();
        }

        [MenuItem(MENU_ITEM + "/XGameObject")]
        public static void CreateXGamepObject()
        {
            CreateCscriptFiles("XGameObjectManager");
            CreateCscriptFiles("XGameObject");
            CreateCscriptFiles("XGameObjectStates");

            AssetDatabase.Refresh();
        }

        [MenuItem(MENU_ITEM + "/FSMMono")]
        public static void CreateFsmMono()
        {
            CreateCscriptFiles("FSMMono");
            CreateCscriptFiles("FSMMonoState");
        }

        [MenuItem(MENU_ITEM + "/HirarchyAccessor")]
        public static void CreateHirarchyAccessor()
        {
            CreateCscriptFiles("HirarchyAccessor", "");
        }
    }
}