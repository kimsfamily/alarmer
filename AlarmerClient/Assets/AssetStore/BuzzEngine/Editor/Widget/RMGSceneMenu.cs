﻿using UnityEditor;

namespace BuzzEngine.Editor.Widget
{
    public class OpenScene 
    {
        //static string findPath = "Assets/RehabMaster";

        //[MenuItem("DGate/MakeActionGuideBundle", priority = 0)]
        //static void MakeActionGuideBundle() 
        //{
        //    BuildPipeline.BuildAssetBundles("BundleTest");
        //}

        [MenuItem("File/Save Project &S", priority = 2)]
        static void SaveProject() {
        
            AssetDatabase.SaveAssets();
        }

        #region RehabMaster
        //[MenuItem("DGate/Scenes/RM_Assessment &1",priority = 0)]
        //static void OpenA1() { BzEditorFunction.OpenScene(findPath + "/Scene/RM_Assessment"); }
        //[MenuItem("DGate/Scenes/RM_Training &2", priority = 0)]
        //static void OpenA2() { BzEditorFunction.OpenScene(findPath + "/Scene/RM_Training"); }
        //[MenuItem("DGate/Scenes/RM_FreeAssessment &3", priority = 0)]
        //static void OpenA3() { BzEditorFunction.OpenScene(findPath + "/Scene/RM_FreeAssessment"); }
        //[MenuItem("DGate/Scenes/RM_Title &`", priority = 0)]
        //static void OpenA4() { BzEditorFunction.OpenScene(findPath + "/Scene/RM_Title"); }
        //[MenuItem("DGate/Scenes/RM_Intro &0", priority = 0)]
        //static void OpenA5() { BzEditorFunction.OpenScene(findPath + "/Scene/RM_Intro"); }
        #endregion

        #region RehabMaster_Games
        //[MenuItem("DGate/Scenes/RMG_GoAway", priority = 1)]
        //static void OpenG1() { BzEditorFunction.OpenScene(findPath + "_Games/RMG_GoAway/RMG_GoAway"); }
        //[MenuItem("DGate/Scenes/RMG_Smash", priority = 1)]
        //static void OpenG2() { BzEditorFunction.OpenScene(findPath + "_Games/RMG_Smash/RMG_Smash"); }
        //[MenuItem("DGate/Scenes/RMG_CatchBug", priority = 1)]
        //static void OpenG3() { BzEditorFunction.OpenScene(findPath + "_Games/RMG_CatchBug/RMG_CatchBug"); }
        //[MenuItem("DGate/Scenes/RMG_4", priority = 1)]
        //static void OpenG4() { BzEditorFunction.OpenScene(findPath + "_Games/RMG_Smash/RMG_4"); }
        //[MenuItem("DGate/Scenes/RMG_5", priority = 1)]
        //static void OpenG5() { BzEditorFunction.OpenScene(findPath + "_Games/RMG_Smash/RMG_5"); }

        //[MenuItem("DGate/Scenes/Analysis", priority = 1)]
        //static void OpenW1() { BzEditorFunction.OpenScene("Assets/Wenect/_Demos/Analysis/Analysis"); }
        //[MenuItem("DGate/Scenes/Avatar", priority = 1)]
        //static void OpenW2() { BzEditorFunction.OpenScene("Assets/Wenect/_Demos/Avatar/Avatar"); }
        //[MenuItem("DGate/Scenes/Captures", priority = 1)]
        //static void OpenW3() { BzEditorFunction.OpenScene("Assets/Wenect/_Demos/Captures/CaptureImage"); }
        //[MenuItem("DGate/Scenes/Cubeman", priority = 1)]
        //static void OpenW4() { BzEditorFunction.OpenScene("Assets/Wenect/_Demos/Cubeman/Cubeman"); }
        //[MenuItem("DGate/Scenes/Engager", priority = 1)]
        //static void OpenW5() { BzEditorFunction.OpenScene("Assets/Wenect/_Demos/Engager/Engager"); }
        //[MenuItem("DGate/Scenes/Gesture", priority = 1)]
        //static void OpenW6() { BzEditorFunction.OpenScene("Assets/Wenect/_Demos/Gesture/Gesture"); }
        //[MenuItem("DGate/Scenes/Interaction", priority = 1)]
        //static void OpenW7() { BzEditorFunction.OpenScene("Assets/Wenect/_Demos/Interaction/Interaction"); }
        //[MenuItem("DGate/Scenes/Skeleton", priority = 1)]
        //static void OpenW8() { BzEditorFunction.OpenScene("Assets/Wenect/_Demos/Skeleton/Skeleton"); }
        ////[MenuItem("DGate/Scenes/Avatar", priority = 1)]
        ////static void OpenW9() { BzEditorFunction.OpenScene("Assets/Wenect/_Demos/Avatar/Avatar"); }
        ////[MenuItem("DGate/Scenes/Avatar", priority = 1)]
        ////static void OpenW10() { BzEditorFunction.OpenScene("Assets/Wenect/_Demos/Avatar/Avatar"); }


        #endregion

    }
}
