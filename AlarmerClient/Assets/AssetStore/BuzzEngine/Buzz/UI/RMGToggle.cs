﻿using UnityEngine;
using UnityEngine.UI;

namespace RehabMaster_Games.AAplication.Buzz.UI
{
    public class RMGToggle : MonoBehaviour
    {
        public Toggle Toggle;
        public Text Text;
        public Color OriginColor { get; set; }
        public Color SelectedLabelColor = new Color(55f / 255f, 55f / 255f, 55f / 255f,1f);

        public int Number;
        public string Label;

        public bool IsOn { get { return Toggle.isOn; } set { Toggle.isOn = value;  } }

        public RMGToggleGroup Group;
        
        public void Awake()
        {
            OriginColor = Text.color;
        }

        public void SetData(int number, string text, bool isNumbering)
        {
            Number = number;
            Label = text;
            if (isNumbering)
                Text.text = Number + "." + text;
            else
                Text.text = text;
        }
        public void Change(Color color) 
        {
            Text.color = color;
        }

        public void ReCover()
        {
            Text.color = OriginColor;
        }

        public void OnChange(bool val)
        {
            if(Group.UseMultiSelect == false)
                Group.AllUnCheck(this);
            
            IsOn = val;

            Change(IsOn ? SelectedLabelColor : OriginColor);
        }
    }
}
