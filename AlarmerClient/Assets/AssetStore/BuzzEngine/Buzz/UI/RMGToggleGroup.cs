﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RehabMaster_Games.AAplication.Buzz.UI
{
    public class RMGToggleGroup : MonoBehaviour
    {
        public enum EToggleType { Example, Numbering}
        public struct AnswerInfo
        {
            public int Index;
            public string Value;
        }
        public RMGToggle Template;
        public EToggleType ToggleType;
        public string Range;

        public bool UseInspectorData = false;
        public bool UseMultiSelect = false;
        public bool UseNumbering = true;

        public List<string> DataList = new List<string>();
        public List<RMGToggle> ToggleList = new List<RMGToggle>();

        public void Awake()
        {
            if (UseInspectorData)
            {
                switch (ToggleType)
                {
                    case EToggleType.Example:
                        CreateToggles(DataList);
                        break;
                    case EToggleType.Numbering:
                        CreateTogglesByNumbering(Range);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            
            Template.gameObject.SetActive(false);
        }
        
        public void CreateTogglesByNumbering(string range)
        {
            string[] DataTemp;
            
            // Range type '1~5'
            
            if (range.Contains('~'))
            {
                var strArr = range.Split('~');
                var startid = int.Parse(strArr[0]);
                var endid = int.Parse(strArr[1]);

                DataTemp = new string[endid];
                for (var i = 0; i <= endid - startid; i++)
                {
                    DataTemp[i] = "" + (i + startid);
                }

                CreateToggles(DataTemp.ToList());

                return;
            }
            // list type '2,3,5'
            else if (range.Contains(','))
            {
                var strArr = range.Split(',');
                DataTemp = new string[strArr.Length];

                for (var i = 0; i < DataTemp.Length; i++)
                {
                    DataTemp[i] = strArr[i];
                }

                CreateToggles(DataTemp.ToList());

                return;
            }
        }

        public void AddToggle(RMGToggle toggle)
        {
            ToggleList.Add(toggle);

            toggle.transform.SetParent(transform, false);
        }

        public void AllUnCheck(RMGToggle seletToggle)
        {
            foreach (var toggle in ToggleList)
            {
                if (toggle != seletToggle)
                {
                    toggle.IsOn = false;
                    toggle.ReCover();
                }
            }           
        }

        public void ClearToggles()
        {
            foreach (var toggle in ToggleList)
            {
                DestroyImmediate(toggle.gameObject);
            }

            ToggleList.Clear();
        }

        public void CreateToggles(List<string> list)
        {
            Template.gameObject.SetActive(true);

            for (var i = 0; i < list.Count; i++)
            {
                var answer = Instantiate(Template);
                answer.SetData(i + 1, list[i], UseNumbering);
                AddToggle(answer);
            }

            Template.gameObject.SetActive(false);
        }
        
        public AnswerInfo[] Value()
        {
            var temp = new List<AnswerInfo>();

            for (var i = 0; i < ToggleList.Count; i++)
            {
                if (ToggleList[i].IsOn)
                    temp.Add(new AnswerInfo() {Index = ToggleList[i].Number, Value = ToggleList[i].Label});
            }
            return temp.ToArray();
        }
    }
}
