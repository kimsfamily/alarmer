﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MergeImage : MonoBehaviour
{
    public Texture2D TexA;
    public Texture2D TexB;

    private Color[] _clrA;
    private Color[] _clrB;
    public Texture2D _texFinal;
    private SpriteRenderer _spriteRenderer;

    // Use this for initialization
    void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();

        _clrA = TexA.GetPixels();
        _clrB = TexB.GetPixels();

        _texFinal = new Texture2D(4096, 4096);
        _texFinal.SetPixels(_clrA);
        _texFinal.Apply();

        _spriteRenderer.sprite = Sprite.Create(_texFinal, new Rect(0, 0, 4096, 4096), new Vector2(0.5f, 0.5f));
        
        merge();
    }

    void merge()
    {
        for (int intY = 0; intY < 4096; intY++)
        {
            for (int intX = 0; intX < 4096; intX++)
            {
                if (intY > 0 && intY < 4096 && intX < 4096)
                {
                    if(_clrA[intX + (intY * 4096)].a<=0)
                   _clrA[intX + (intY * 4096)] = _clrB[intX + (intY * 4096)];
                }
            }

        }
        
        _texFinal.SetPixels(_clrA);
        _texFinal.Apply();

    }

    }

