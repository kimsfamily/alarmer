﻿using UnityEngine;
using System.Collections;

namespace BuzzEngine{
	public class BzPGameObject : MonoBehaviour {

		public ParticleSystem particle;

		public void PlayParticle(){
			PlayParticle(Vector3.zero, true);
		}	
		public void PlayParticle(Vector3 pos){
			PlayParticle(pos, true);
		}	
		public void PlayParticle(Vector3 pos, bool withChildren) {
			if( particle != null ){
				particle.Simulate(0);
				particle.Play(withChildren);
				StartCoroutine( AutoDisable() );
			}else{
				print("Error : Not Particle");
			}
		}

		public bool IsPlayParticle(){
			if( particle != null ){
				return particle.isPlaying;
			}else{
				return false;
			}
		}
		// 파티클의 진행 상태를 검사하여 현재 종료 되었다면 자동으로 꺼버린다. 
		IEnumerator AutoDisable(){
			while (particle.isPlaying){
				yield return new WaitForSeconds(1f);
				if( ! particle.isPlaying ){
					gameObject.SetActive(false);
					StopAllCoroutines();
				}
			}
		}
	}
}