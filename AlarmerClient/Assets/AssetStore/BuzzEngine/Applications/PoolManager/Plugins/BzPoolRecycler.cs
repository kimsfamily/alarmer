﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class BzPoolRecycler : MonoBehaviour
{
    public enum StopType
    {
        None, Multi, Animation, Particle,
    }public StopType stopType = StopType.None;

    public ParticleSystem  particle = null;
    public Animation       myAnimation = null;

    void Awake()
    {
        if (myAnimation == null)
        {
            myAnimation = gameObject.GetComponent<Animation>();

        }
        if (particle == null)
        {
            particle = gameObject.GetComponent<ParticleSystem>();

        }

        if (myAnimation != null)
        {
            stopType = StopType.Animation;
        }

        if (particle != null)
        {
            stopType = StopType.Particle;
        }
    }
    void OnEnable()
    {
        if (myAnimation != null)
        {
            myAnimation.CrossFade(myAnimation.clip.name);
            stopType = StopType.Animation;
        }

        if (particle != null)
        {
            particle.Play(true);
            StartCoroutine(Init());
        }

        if (particle == null && myAnimation == null) Debug.LogError(name + " : Not Found Stop Type");
    }

    void OnDisable()
    {
        if (myAnimation != null)
        {
            myAnimation.Stop(myAnimation.clip.name);
        }

        if (particle != null)
        {
            particle.Stop(true);
        }

        if (particle == null && myAnimation == null) Debug.LogError(name + " : Not Found Stop Type");
    }

    IEnumerator Init()
    {
        yield return new WaitForEndOfFrame();

        if (particle != null)
        {
            particle.Play(true);
        }
    }
   
	// Update is called once per frame
	void FixedUpdate () {
        if (Application.isPlaying == false) return;

        switch (stopType)
        {
            case StopType.Multi:
                if (!particle.isPlaying && !myAnimation.isPlaying)
                {
                    particle.Stop(true);
                    myAnimation.Stop();
                    gameObject.SetActive(false);
                }
                break;
            case StopType.Animation:
                if (!myAnimation.isPlaying)
                {
                    myAnimation.Stop();
                    gameObject.SetActive(false);
                }
                break;
            case StopType.Particle:
                if (!particle.isPlaying)
                {
                    particle.Stop(true);
                    gameObject.SetActive(false);
                }
                break;
        }
	}
}
