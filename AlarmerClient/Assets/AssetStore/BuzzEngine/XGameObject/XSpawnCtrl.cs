using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BuzzEngine.XGameObject
{
    public abstract class XSpawnCtrl : MonoBehaviour
    {
        /// <summary>
        /// 스폰들의 상위 부모. 자동삽입을 위해 존재
        /// </summary>
        public GameObject spawnsParent;
        /// <summary>
        /// 씬에 존재하는 모든 스폰들.
        /// </summary>
        public XSpawn[] allSpawns;

        /// <summary>
        /// 실제 사용하게 될 스폰들. => allSpawn의 필터된 스폰들.
        /// </summary>
        //[HideInInspector]
        public List<XSpawn> useSpawns = new List<XSpawn>();

        public bool isInitalize = false;

        protected abstract IEnumerator iInitialize();
        public abstract void CreateObject();        
        
        public Coroutine Initialize()
        {
            return StartCoroutine(iInitialize());
        }
        
        void Update()
        {
            if(isInitalize && useSpawns != null && useSpawns.Count > 0)
                CreateObject();
        }

#if UNITY_EDITOR
        [System.Serializable]
        public class SpawnGizmo
        {
            public bool use = true;
            public float size = 1f;
            public Color color = Color.white;
        }
        public SpawnGizmo gizmo = new SpawnGizmo();

        void OnDrawGizmos()
        {
            if (gizmo.use == false) return;

            if (allSpawns == null) allSpawns = null;

            if (allSpawns != null && allSpawns.Length == 0 && spawnsParent != null)
            {
                XSpawn[] spawnsOnChild = spawnsParent.GetComponentsInChildren<XSpawn>();
                allSpawns = new XSpawn[spawnsOnChild.Length];
                for (int i = 0; i < allSpawns.Length; i++)
                {
                    allSpawns[i] = spawnsOnChild[i];
                }
            }
            if (allSpawns != null)
            {
                Gizmos.color = gizmo.color;
                for (int i = 0; i < allSpawns.Length; i++)
                {
                    Gizmos.DrawCube(allSpawns[i].transform.position, Vector3.one * gizmo.size);
                }
            }
        }
#endif

        public void InitUseSpawns()
        {
            useSpawns.Clear();

            for (int i = 0; i < allSpawns.Length; ++i)
            {
                // 데이터 길이가 맞지 않다면 게이트를 닫는다.
                if (allSpawns[i].isOpenGate )
                {
                    useSpawns.Add(allSpawns[i]);
                }                
            }

            orderArray = null;

            ResetOrderArray();
        }
        public void InitUseSpawns(bool[] openInfo)
        {
            if (allSpawns.Length > openInfo.Length)
            {
                Debug.LogError("Check GameOption.JSON Invalid OpenMissions : Resource " + allSpawns.Length + " OpenInfo " + openInfo.Length);
            }

            for (int i = 0; i < allSpawns.Length; ++i)
            {
                // 데이터 길이가 맞지 않다면 게이트를 닫는다.
                if (i >= openInfo.Length)
                {
                    allSpawns[i].isOpenGate = false;
                }
                // 정상일 경우 스폰 등록
                else
                {
                    allSpawns[i].isOpenGate = openInfo[i];
                }
            }

            InitUseSpawns();
        }

        public int GetCountOfSpawnChild()
        {
            int sum = 0;
            for (int i = 0; i < allSpawns.Length; i++)
            {
                sum += allSpawns[i].ChildCount;
            }
            return sum;
        }
        /// <summary>
        /// 확률적으로(1/n) 사용할 스폰 아이디를 구한 다음 해당 아이디가 가르키는 스폰을 리턴한다.
        /// <para>1/n화 시킨 배열에서 가져온다.</para>
        /// </summary>
        /// <returns></returns>
        public XSpawn GetRandomSpawn()
        {
            int asize = useSpawns.Count;
            
            if (orderArrayIndex >= asize)
            {
                ResetOrderArray();
                orderArrayIndex = 0;
            }

            return useSpawns[orderArray[orderArrayIndex++]];
        }
        int[] orderArray;
        int orderArrayIndex;
        /// <summary>
        /// orderArray 에 0 부터 스폰 숫자만큼 정수를 넣고 무작위로 섞는다.
        /// <para>스폰 활용의 확률을 1/N로 만들기위 한 함수</para>
        /// </summary>
        public void ResetOrderArray()
        {
            int asize = useSpawns.Count;

            if (asize == 0) return;

            // 마지막과 겹치지 않게 하기 위해 예외 처리
            int lastid = 0;

            if (orderArray != null && orderArray.Length > 0) lastid = orderArray[asize - 1];

            orderArray = new int[asize];

            for (int i = 0; i < asize; i++)
            {
                orderArray[i] = i;
            }

            for (int i = 0; i < asize; i++)
            {
                int changeId = Random.Range(i, asize);

                int tValue = orderArray[i];
                orderArray[i] = orderArray[changeId];
                orderArray[changeId] = tValue;
            }

            // 섞기전 마지막 값과 섞은 이후 처음 값이 같다면 처음과 마지막을 바꿔 준다.
            if (orderArray[0] == lastid)
            {
                orderArray[0] = orderArray[asize - 1];
                orderArray[asize - 1] = lastid;
            }
        }
    }
}