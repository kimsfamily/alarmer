using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BuzzEngine.XGameObject
{
    public class XSpawn : MonoBehaviour
    {
        public Transform target;

        public float gizmoRadius = 0.3f;
        /// <summary>
        /// 본 스폰을 통해 생성된 오브젝트를 수집한다.
        /// </summary>
        List<XGameObject> childList = new List<XGameObject>();

        public bool isOpenGate { get { return gameObject.activeSelf; } set { gameObject.SetActive(value); } }
        public void RegistXGameObject(XGameObject character)
        {
            childList.Add(character);
        }
        public void UnRegistXGameObject(XGameObject character)
        {            
            childList.Remove(character);
        }

        void OnDrawGizmosSelected()
        {
            Gizmos.DrawSphere(transform.position, gizmoRadius);

            if (null != target && isOpenGate)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(target.position, this.transform.position);
            }
        }
        public int ChildCount
        {
            get
            {
                int count = 0; 
                for( int i = childList.Count - 1; i >= 0; i--)
                {
                    if( childList[i] == null)
                    {
                        childList.RemoveAt(i); 
                    }
                    else
                    {
                        count++;
                    }
                }

                return count;
            }
        }
    }
}
