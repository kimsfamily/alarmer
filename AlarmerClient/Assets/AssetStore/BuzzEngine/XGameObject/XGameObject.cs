﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using BuzzEngine;

using eTeam = BuzzEngine.XGameObject.XGameObjectManager.eTeam;

namespace BuzzEngine.XGameObject
{
    /// <summary>
    /// 하나의 단일 생명체(오브젝트)를 지칭하는 클래스
    /// 
    /// </summary>
    public abstract class XGameObject : MonoBehaviour
    {
        /// <summary>
        ///  XGameObject를 등록 관리 하는 매니져 클래스
        ///  <para>캐릭터가 하이라키에 생성시 자동 등록 될수 있도록 OnEnable()에 등록 시켜 준다.</para>
        /// </summary>
        public XGameObjectManager controler = null;
        /// <summary>
        /// 현재 스테이트의 키를 가르킨다.
        /// <para>SetState(string _stateKey)를 통해 변경 가능하다.</para>
        /// </summary>
        [SerializeField]
        private string currentStateKey = "None";
        /// <summary>
        /// 사용할 State들을 관리 하기 위한 저장소
        /// </summary>
        Dictionary<string, XGameObjectState> dicState = new Dictionary<string, XGameObjectState>();
        /// <summary>
        /// 현 오브젝트의 팀네임(주로 캐릭터에 활용시 사용함)
        /// <para>ex) Enemy, Player</para>
        /// </summary>
        public XGameObjectManager.eTeam teamKey = XGameObjectManager.eTeam.Enemy;
        XSpawn _spawn_ = null;
        /// <summary>
        /// 모든 오브젝트는 자기가 태어난 spawn의 정보를 가지고 있다.
        /// </summary>
        /// <param name="pObject"></param>
        public XSpawn spawn { get { return _spawn_; } set { _spawn_ = value; _spawn_.RegistXGameObject(this); } }
        
        /// <summary>
        /// 등록된 모든 스테이트에 초기화를 시켜준다.
        /// </summary>
        /// <param name="pObject"></param>
        public void InitStates(XGameObject pObject)
        {
            foreach (var key in dicState) StartCoroutine(key.Value.Initialize());
        }
        /// <summary>
        /// State를 등록 한다.
        /// </summary>
        public abstract void Awake();
        /// <summary>
        /// XGameObjectCtrl에 등록 한다.
        /// </summary>
        public abstract void OnEnable();
        /// <summary>
        /// Manager 해제 한다.
        /// </summary>
        public abstract void OnDisable();
        /// <summary>
        /// 현재 State의 Execute 를 수행한다.
        /// </summary>
        public void Update()
        {
            if (currentStateKey == "None") return;

            if (dicState.ContainsKey(currentStateKey))
                GetCurrentState().Update();
            else
                return;
        }
        /// <summary>
        /// 같은 팀인지를 여부를 리턴한다.
        /// <para>인자는 하위 클래스에서 정의한 enum값을 스트링으로 변환후 (string)team과 비교 한다.</para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public bool EqualTeam(XGameObjectManager.eTeam t)
        {
            return teamKey.Equals(t);
        }
        /// <summary>
        /// stateList에 State를 추가 한다.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="_XGameObject"></param>
        public void AddStateDic<T>(T t, XGameObjectState _XGameObject)
        {
            if (dicState.ContainsKey(t.ToString()))
                Debug.LogError("An element with the same key already exists in the dictionary." + t.ToString());
            else
                dicState.Add(t.ToString(), _XGameObject);
        }
        /// <summary>
        /// 등록된 스테이트 중 특정 스테이트를 리턴한다.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public XGameObjectState GetStateFromStateDic<T>(T t)
        {
            if (dicState.ContainsKey(t.ToString()))
                return dicState[t.ToString()];
            else
            {
                Debug.LogError("Invalid key : " + t.ToString());
                return null;
            }
        }
        /// <summary>
        /// 스테이트 비교용 편의 함수
        /// <para>인자는 하위 클래스에서 정의한 enum값을 스트링으로 변환후 (string)team과 비교 한다.</para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public bool EqualCurrentState<T>(T t)
        {
            if (currentStateKey == "None") return false;

            if (GetCurrentState() != null) return GetCurrentStateKey().Equals(t.ToString());// GetCurrentState().Equals(t);
            return false;
        }
        /// <summary>
        /// stateList에 등록된 state들 중 구동될 아이디를 세팅 한다.
        /// <para>대입된 인덱스에 때라 current State 가 결정되며 Update를 통해 구동된다.</para>
        /// <para>GetCurrentState()를 통해 참조 할수있다.</para>
        /// <para>동일한 state 대입에 대한 중복 처리가 막혀있다.</para>
        /// <para>중요 : state에서 정의한 IsEnterable 에 따라 대입이 방지 될수도 있다.</para>
        /// </summary>
        /// <param name="_stateKey"></param>
        public void SetCurrentState<T>(T t)
        {
            string _stateKey = t.ToString();
            if (_stateKey == "None") return;

            if (dicState.ContainsKey(_stateKey) == false)
            {
                Debug.LogError("Not Found State ID " + BzLog.WrapColor(_stateKey, Color.red) + " Name:" + name + " Team:" + teamKey);
                return;
            }
            // Enter함수에서 조건에 부합 하지 않을 경우 해당 state로 진입 하지 않고 현재에 머물러 있는다.
            if (dicState[_stateKey].IsEnterable(this))
            {
                // 현재 스테이트 종료
                if (dicState.ContainsKey(currentStateKey))
                    dicState[currentStateKey].Exit();

                currentStateKey = _stateKey;


                // 신규 스테이트 시작
                dicState[currentStateKey].Enter(this);
            }
        }
        /// <summary>
        /// 현재 스테이트 키를 리턴한다.
        /// <para>GetCurrentState()를 통해 접근 할수도 있다.</para>
        /// </summary>
        /// <returns></returns>
        public string GetCurrentStateKey()
        {
            return currentStateKey;
        }
        /// <summary>
        /// 현재 구동되는 State를 리턴한다.
        /// </summary>
        /// <returns></returns>
        public XGameObjectState GetCurrentState()
        {
            return dicState[currentStateKey];
        }

        void OnCollisionEnter2D(Collision2D collsion)
        {
            GetCurrentState().OnCollisionEnter2D(collsion);
        }
        void OnCollisionStay2D(Collision2D collsion)
        {
            GetCurrentState().OnCollisionStay2D(collsion);
        }
        void OnCollisionExit2D(Collision2D collsion)
        {
            GetCurrentState().OnCollisionExit2D(collsion);
        }
        void OnTriggerEnter2D(Collider2D collider)
        {
            GetCurrentState().OnTriggerEnter2D(collider);
        }
        void OnTriggerStay2D(Collider2D collider)
        {
            GetCurrentState().OnTriggerStay2D(collider);
        }
        void OnTriggerExit2D(Collider2D collider)
        {
            GetCurrentState().OnTriggerExit2D(collider);
        }

        void OnCollisionEnter(Collision collsion)
        {
            GetCurrentState().OnCollisionEnter(collsion);
        }
        void OnCollisionStay(Collision collsion)
        {
            GetCurrentState().OnCollisionStay(collsion);
        }
        void OnCollisionExit(Collision collsion)
        {
            GetCurrentState().OnCollisionExit(collsion);
        }
        void OnTriggerEnter(Collider collider)
        {
            GetCurrentState().OnTriggerEnter(collider);
        }
        void OnTriggerStay(Collider collider)
        {
            GetCurrentState().OnTriggerStay(collider);
        }
        void OnTriggerExit(Collider collider)
        {
            GetCurrentState().OnTriggerExit(collider);
        }
        void OnDrawGizmos()
        {
            if (GetCurrentStateKey() != "None")
                GetCurrentState().OnDrawGizmos();
        }
        void OnDrawSelectedGizmos()
        {
            if( GetCurrentStateKey() != "None")
                GetCurrentState().OnDrawSeletedGizmos();
        }
    }

}