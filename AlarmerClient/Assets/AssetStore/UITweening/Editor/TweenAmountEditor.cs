using UnityEngine;
using UnityEditor;
using System.Collections;

namespace UnityEngine.UI
{
    [CustomEditor(typeof(TweenAmount))]
    public class TweenImageEditor : TweenMainEditor
    {
        public override void OnInspectorGUI()
        {
            TweenAmount self = (TweenAmount)target;

            EditorGUILayout.BeginHorizontal();
            self.from = EditorGUILayout.Slider("From", self.from, 0, 1);
            if (GUILayout.Button("\u25C0", GUILayout.Width(24f)))
                self.FromCurrentValue();
            EditorGUILayout.EndHorizontal();
            self.fromOffset = EditorGUILayout.Toggle("Offset", self.fromOffset);

            EditorGUILayout.BeginHorizontal();
            self.to = EditorGUILayout.Slider("To", self.to, 0, 1);
            if (GUILayout.Button("\u25C0", GUILayout.Width(24f)))
                self.ToCurrentValue();
            EditorGUILayout.EndHorizontal();
            self.toOffset = EditorGUILayout.Toggle("Offset", self.toOffset);

            BaseTweenerProperties();
        }
    }
}
