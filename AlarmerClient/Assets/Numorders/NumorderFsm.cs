using System.Collections;

namespace Numorders
{
    public class NumorderFsm : BuzzEngine.FSM.FSMMono<NumorderFsm>
    {
        public enum eState
        {
            TitlePatch, Title, Lobby, Survival,SurvivalRank,
        }

        public eState StartState;
        
        IEnumerator Start()
        {
            yield return null;
            ChangeState(this, StartState);
        }

        void Update()
        {
            
        }
    }
}
