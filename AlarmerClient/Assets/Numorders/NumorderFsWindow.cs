﻿using System.Collections;
using UnityEngine;

namespace Numorders
{
    public class NumorderFsWindow : NumorderFsState
    {
        public NumorderFsm AppFsm { get { return global::Numorders.Numorders.i.AppFsm; } }

        /// <summary>
        /// time 이후에 객체를 소멸한다.
        /// </summary>
        public Coroutine ReserveDestroy(float time)
        {
            return StartCoroutine(iReserveDestroy(time));
        }
        /// <summary>
        /// time 이후에 객체를 소멸한다.
        /// </summary>
        IEnumerator iReserveDestroy(float time)
        {
            yield return new WaitForSeconds(time);

            DestroyImmediate(gameObject);
        }
    }
}
