using UnityEngine;
using BuzzEngine;
using System.Collections;
using eState = Alarmer.PatchSystem.PatchSystemFSMMono.eState;

namespace Alarmer.PatchSystem
{
    public abstract class PatchSystemFSMMonoState : BuzzEngine.FSM.FSMMonoState<PatchSystemFSMMono>
    {
        public override IEnumerator Initialize()
        {
            return base.Initialize();
        }
    }

    public class Load : PatchSystemFSMMonoState
    {
        IEnumerator Start()
        {
            ChangeState(eState.Idle);
            return base.Initialize();
        }
    }

    public class Idle : PatchSystemFSMMonoState
    {
        void Start()
        {
            ChangeState(eState.IloveYou);
        }
    }

    public class IloveYou : PatchSystemFSMMonoState
    {
        void Start()
        {
            Debug.Log(this);
        }
    }
}
