using System.Collections;
using UnityEngine;

namespace Numorders
{
    public class TitlePatch : NumorderFsWindow
    {
        IEnumerator Start()
        {
            ChangeState(NumorderFsm.eState.Title);
            yield return new WaitForSeconds(2f);
            yield return base.Initialize();
        }
    }
}