using UnityEngine;
using BuzzEngine;
using System.Collections;

namespace Alarmer.PatchSystem
{
    public class PatchSystemFSMMono : BuzzEngine.FSM.FSMMono<PatchSystemFSMMono>
    {
        public enum eState
        {
            Load,Idle,IloveYou
        }

        public eState StartState;
        
        IEnumerator Start()
        {
            yield return null;
            ChangeState(this, StartState);
        }

        void Update()
        {
            
        }
    }
}
