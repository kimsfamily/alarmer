using System.Collections;
using Alarmer.PatchSystem;
using UnityEngine;

namespace Numorders
{
    public abstract class NumorderFsState : BuzzEngine.FSM.FSMMonoState<NumorderFsm>
    {
        public override IEnumerator Initialize()
        {
            return base.Initialize();
        }
    }

    public class Exit : NumorderFsState
    {
        IEnumerator Start()
        {
            ChangeState(NumorderFsm.eState.Title);
            yield return new WaitForSeconds(2f);
            yield return base.Initialize();
        }
    }
}
