﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WebCamTest : MonoBehaviour
{
    public WebCamTexture mCamera = null;
    public RawImage Picture;

    // Use this for initialization
    void Start()
    {
        Debug.Log("Script has been started");

        mCamera = new WebCamTexture();
        Picture.texture = mCamera;
        mCamera.Play();

    }

    // Update is called once per frame
    void Update()
    {

    }
}
